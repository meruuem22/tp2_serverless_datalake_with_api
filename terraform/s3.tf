# TODO : Create a s3 bucket with aws_s3_bucket

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket" "b" {
  bucket = "s3-job-offer-bucket-anassugotea"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
 
  force_destroy = true
}

resource "aws_s3_bucket_object" "job_offers" {
    bucket = "s3-job-offer-bucket-anassugotea"
    acl    = "private"
    key    = "job_offers/raw/"
    source = "/dev/null"
}


resource "aws_lambda_permission" "allow_bucket1" {
  statement_id  = "AllowExecutionFromS3Bucket1"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.func1.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.b.arn
}

resource "aws_lambda_function" "func1" {
  filename      = "empty_lambda_code.zip"
  function_name = "example_lambda_name1"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
}
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "s3-job-offer-bucket-anassugotea"

  lambda_function {
    lambda_function_arn = aws_lambda_function.func1.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [
    aws_lambda_permission.allow_bucket1
  ]
}
